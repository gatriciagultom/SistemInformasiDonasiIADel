package org.psi.controllers;


import javax.validation.Valid;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.psi.dao.AkunDao;
import org.psi.dao.ArtikelDao;
import org.psi.dao.KeuanganDao;
import org.psi.dao.DonasiDao;
import org.psi.models.Akun;
import org.psi.models.Artikel;
import org.psi.models.Donasi;
import org.psi.models.DonasiBarang;
import org.psi.models.DonasiUang;
import org.psi.models.Keuangan;
import org.psi.repository.DonasiBarangRepository;
import org.psi.repository.DonasiUangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {

	private AkunDao aDao;
	private DonasiDao tDao;
	private Akun akunLogin;
	private KeuanganDao kDao;
	@Autowired
	private ArtikelDao dDao;

	@Autowired
	private DonasiUangRepository durepo;
	@Autowired
	private DonasiBarangRepository dbrepo;
	@Autowired
	public ArtikelDao getdDao() {
		return dDao;
	}
	@Autowired
	public void setdDao(ArtikelDao dDao) {
		this.dDao = dDao;
	}
	@Autowired
	public void setuDao(AkunDao aDao) {
		this.aDao = aDao;
	}
	@Autowired
	public void setkDao(KeuanganDao kDao) {
		this.kDao = kDao;
	}
	@Autowired
	public void settDao(DonasiDao tDao) {
		this.tDao = tDao;
	}
	public AkunDao getaDao() {
		return aDao;
	}
	public void setaDao(AkunDao aDao) {
		this.aDao = aDao;
	}
	public DonasiDao gettDao() {
		return tDao;
	}

	public Akun getAkunLogin() {
		return akunLogin;
	}
	public void setAkunLogin(Akun akunLogin) {
		this.akunLogin = akunLogin;
	}
	public KeuanganDao getkDao() {
		return kDao;
	}


	@RequestMapping({"/psi", "/"})
	public String index(Model model, HttpServletRequest req) {
		if(akunLogin!=null){
			model.addAttribute("akunLogin", (Akun)req.getSession().getAttribute("akunLogin"));
		}else{
			model.addAttribute("akunLogin",new Akun());
		}
		return "index";
	}

	@RequestMapping("/403")
	public String AccessDenied(Model model) {
		model.addAttribute("akun", new Akun());
		model.addAttribute("akunLogin",new Akun());
		return "403";
	}


	@RequestMapping("/login")
	public String login(Model model)
	{	
		if(akunLogin == null) {
			model.addAttribute("akun", new Akun());
			model.addAttribute("akunLogin",new Akun());
			return "login";
		}
		return "redirect:/";
	}
	
	@GetMapping("/daftar")
	public String daftar(Model model)
	{	
		model.addAttribute("akun", new Akun());
		model.addAttribute("akunLogin",new Akun());
		return "/register/register";
		
	}
	
	@RequestMapping(value = "/createAkun", method = RequestMethod.POST)
	public String saveOrUpdateAkun(Model model, @Valid Akun akun, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			return "/register/register";
		}
		
		model.addAttribute("akun", aDao.saveOrUpdate(akun));
		return "redirect:/";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(Model model, HttpServletRequest request) {
		String username = request.getParameter("user");
		String password = request.getParameter("pass");
		if(aDao.login(username, password) != null){
			request.getSession().setAttribute("akunLogin", aDao.login(username, password));
			akunLogin = (Akun) request.getSession().getAttribute("akunLogin");
			if (akunLogin.getRole().equals("Donatur")) {
				return "redirect:/psi";
			} else if (akunLogin.getRole().equals("Sukarelawan")) {
				return "redirect:/psi";
			}  else if (akunLogin.getRole().equals("Admin")) {
				return "redirect:/psi";
			}	
		}
		return "redirect:/login";
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request){
		request.getSession().removeAttribute("akunLogin");
		akunLogin = null;
		//		request.getSession().invalidate();
		return "redirect:/psi";
	}

	@RequestMapping(value="/psi/donatur/createDonasi",  method= RequestMethod.GET)
	public String homeDonatur(Model model) {
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Donatur")) {
				Donasi donasi = new Donasi();
				/*donasi.setId_donatur(akunLogin.getId());*/
				/*donasi.setNama_(akunLogin.getUsername());*/
				/*donasi.setStatus("Undone");*/
				model.addAttribute("ifEdit", false);
				model.addAttribute("donasi", donasi);
				model.addAttribute("listdonasi",tDao.getAllDonasiByStatus("Undone"));
				model.addAttribute("allDonasi", tDao.getAllDonasiByIdDonatur(akunLogin.getId()));
				model.addAttribute("akunLogin",akunLogin);
				return "donasi";
			}
			model.addAttribute("akunLogin",akunLogin);
			return"403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}
	
	@RequestMapping(value="/psi/donatur/createDonasi",  method= RequestMethod.POST)
	public String saveDonatur(Model model, Donasi donasi) {
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Donatur")) {
		
				model.addAttribute("donasi", tDao.saveOrUpdate(donasi));
				
				model.addAttribute("akunLogin",akunLogin);
				return "redirect:/psi/donatur/createDonasi";
			}
			model.addAttribute("akunLogin",akunLogin);
			return"403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}
	
	
	@RequestMapping(value = "/psi/donatur/edit/{id}", method = RequestMethod.GET)
	public String editDonasi(@PathVariable Integer id,  Model model){
		if(akunLogin != null) {
			if(akunLogin.getRole().equalsIgnoreCase("keuangan")) {
				model.addAttribute("donasi", tDao.findDonasiById(id));
				model.addAttribute("allDonasi", tDao.getAllDonasiByIdDonatur(akunLogin.getId()));
				model.addAttribute("akunLogin",akunLogin);
				return "donasi";
			}
			
			return "403"; 
		}
		return "403";
	}

	@RequestMapping("/psi/donatur/delete/{id}")
	public String deleteDonasi(@PathVariable String id, Model model){
		if(akunLogin != null) {
			if(akunLogin.getRole().equalsIgnoreCase("donasi")) {
				tDao.deleteDonasiById(Integer.parseInt(id));
				return "redirect:/psi/donatur";
			} 
			model.addAttribute("akunLogin",akunLogin);
			return "403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}


	@RequestMapping(value="/createKeuangan", method= RequestMethod.GET)
	public String homeAdmin(Model model) {
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {
				Keuangan keuangan= new Keuangan();
				keuangan.setId_admin(akunLogin.getId());
				/*keuangan.setNama_admin(akunLogin.getUsername());*/
				keuangan.setStatus("Undone");
				model.addAttribute("ifEdit", false);
				model.addAttribute("keuangan", keuangan);
				model.addAttribute("allKeuangan", kDao.getAllKeuanganByIdAdmin(akunLogin.getId()));
				model.addAttribute("akunLogin",akunLogin);
				return "keuangan";
			}
			model.addAttribute("akunLogin",akunLogin);
				return "403";
			}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}

	@RequestMapping(value = "/createKeuangan/edit/{id}", method = RequestMethod.GET)
	public String editkeuangan(@PathVariable Integer id,Model model){
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {

				model.addAttribute("ifEdit", true);
				model.addAttribute("keuangan", kDao.findKeuanganById(id));
				model.addAttribute("allKeuangan", kDao.getAllKeuanganByIdAdmin(akunLogin.getId()));
				model.addAttribute("akunLogin",akunLogin);
				return "keuangan";
			}
			model.addAttribute("akunLogin",akunLogin);
			return "403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}

	@RequestMapping("/createKeuangan/delete/{id}")
	public String deletekeuangan(@PathVariable String id, Model model){
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {
				kDao.deleteKeuanganById(Integer.parseInt(id));
				return "redirect:/createKeuangan";
			}
			model.addAttribute("akunLogin",akunLogin);
			return"403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}

	@RequestMapping(value={"/artikel"}, method = RequestMethod.GET)
	public String artikel(Model model){
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {
				model.addAttribute("akunLogin",akunLogin);
				//model.addAttribute("artikel",new Artikel());
				model.addAttribute("list_artikel",dDao.getAllArtikel());
				return "list_artikel";
			}
			model.addAttribute("akunLogin",akunLogin);
			return "403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}


	@RequestMapping(value={"/artikel"}, method = RequestMethod.POST)
	public String saveArtikel(Model model,Artikel artikel){
		System.out.println("-->"+artikel.getJudul_artikel());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {
				model.addAttribute("akunLogin",akunLogin);
				model.addAttribute("artikel",dDao.saveOrUpdate(artikel));
				return "redirect:/psi";
			}
			model.addAttribute("akunLogin",akunLogin);
			return "403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}
	
	/*@RequestMapping(value={"/list_artikel"})
	public String listArtikel(Model model){
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("admin")) {
				model.addAttribute("akunLogin",akunLogin);
				model.addAttribute("Admin",dDao.getAllArtikel());
				return "list_artikel";
			}
			return "403";
		}
		return "403";
	}
	*/
	
	@RequestMapping(value={"/homeSukarelawan"})
	public String homeSukarelawan(Model model){
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Sukarelawan")) {
				model.addAttribute("akunLogin",akunLogin);
				model.addAttribute("barang",dbrepo.findAll());
				model.addAttribute("uang", durepo.findAll());
				return "homeSukarelawan";
			}
			model.addAttribute("akunLogin",akunLogin);
			return "403";
		}
		
		return "403";
	}
	
	
	@RequestMapping(value={"/updateDonasi/{id}"})
	public String konfirmasiSukarelawan(Model model,Donasi donasi, @PathVariable Integer id){
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Sukarelawan")) {
				model.addAttribute("akunLogin",akunLogin);
				tDao.konfirmasi(id);
				return "redirect:/homeSukarelawan";
			}
			return "403";
		}
		return "403";
	}

	@RequestMapping("/psi/addsukarelawan")
	public String addsukarelawan(){
		return "addsukarelawan";
	}

	@RequestMapping(value={"/view-artikel/{id}"})
	public String viewArtikel(Model model, @PathVariable Integer id){
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {
				model.addAttribute("akunLogin",akunLogin);
				model.addAttribute("artikel", dDao.findArtikelById(id));
				
				return "lihat_artikel";
			}
			return "403";
		}
		return "403";
	}

	@RequestMapping(value = "/createArtikel")
	public String createArtikel(Model model, Artikel artikel){
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {
				model.addAttribute("akunLogin",akunLogin);
				return "artikel";
			}
			model.addAttribute("akunLogin",akunLogin);
			return "403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}
	
	
	@RequestMapping(value = "/createArtikel/edit/{id}", method = RequestMethod.GET)
	public String editartikel(@PathVariable Integer id,Model model){
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {

				/*model.addAttribute("ifEdit", true);*/
				model.addAttribute("artikel", dDao.findArtikelById(id));
				model.addAttribute("allArtikel", dDao.getAllArtikelByIdAdmin(akunLogin.getId()));
				model.addAttribute("akunLogin",akunLogin);
				return "artikel";
			}
			model.addAttribute("akunLogin",akunLogin);
			return "403";
		}
		
		return "403";
	}

	@RequestMapping("/createArtikel/delete/{id}")
	public String deleteartikel(@PathVariable String id, Model model){
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Admin")) {
				dDao.deleteArtikelById(Integer.parseInt(id));
				return "redirect:/artikel";
			}
			model.addAttribute("akunLogin",akunLogin);
			return"403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}

	@RequestMapping(value="/donasibarang",  method= RequestMethod.GET)
	public String homeBarang(Model model,HttpServletRequest req , DonasiBarang db) {
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Donatur")) {
				model.addAttribute("ifEdit", false);
				model.addAttribute("listdonasi",dbrepo.findByiddonatur(akunLogin.getId()));
				model.addAttribute("allDonasi", tDao.getAllDonasiByIdDonatur(akunLogin.getId()));
				model.addAttribute("akunLogin",akunLogin);
				return "donasi/donasibarang";
			}
			model.addAttribute("akunLogin",akunLogin);
			return"403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}
	@RequestMapping(value="/donasiuang",  method= RequestMethod.GET)
	public String homeDonaturUang(Model model,HttpServletRequest req ) {
		System.out.println("->"+akunLogin.getRole());
		if(akunLogin != null) {
			if(akunLogin.getRole().equals("Donatur")) {
				model.addAttribute("ifEdit", false);
				model.addAttribute("listdonasi",durepo.findByiddonatur(akunLogin.getId()));
				model.addAttribute("allDonasi", tDao.getAllDonasiByIdDonatur(akunLogin.getId()));
				model.addAttribute("akunLogin",akunLogin);
				return "donasi/donasiuang";
			}
			model.addAttribute("akunLogin",akunLogin);
			return"403";
		}
		model.addAttribute("akunLogin",akunLogin);
		return "403";
	}
}
