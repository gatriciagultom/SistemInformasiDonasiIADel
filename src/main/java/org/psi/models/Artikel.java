package org.psi.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Artikel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int id_admin;
	
	private String nama_admin;
	private String judul_artikel;
	private String isi_artikel;
	private String date_created;
	public int getId() {
		return id;
	}
	
	public int getId_admin() {
		return id_admin;
	}
	public String getNama_admin() {
		return nama_admin;
	}
	
	public void setId_admin(int id_admin) {
		this.id_admin = id_admin;
	}
	public void setNama_admin(String nama_admin) {
		this.nama_admin = nama_admin;
	}
	
	public void setId(int id) {
		this.id = id;
	}
		public String getJudul_artikel() {
		return judul_artikel;
	}
	public void setJudul_artikel(String judul_artikel) {
		this.judul_artikel = judul_artikel;
	}
	public String getIsi_artikel() {
		return isi_artikel;
	}
	public void setIsi_artikel(String isi_artikel) {
		this.isi_artikel = isi_artikel;
	}
	public String getDate_created() {
		return date_created;
	}
	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}

}
