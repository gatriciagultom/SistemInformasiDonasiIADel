package org.psi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "donasi_barang")
public class DonasiBarang {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "alamat")
	private String alamat;
	
	@Column(name = "id_donatur")
	private int iddonatur;
	
	@Column(name = "jumlah_barang")
	private String jumlahbarang;
	
	@Column(name = "nama_barang")
	private String namabarang;
	
	@Column(name = "nama_lengkap")
	private String namalengkap;
	
	@Column(name = "no_telp")
	private String notelp;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "tanggal_penjemputan")
	private String tanggal;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public int getIddonatur() {
		return iddonatur;
	}

	public void setIddonatur(int iddonatur) {
		this.iddonatur = iddonatur;
	}

	public String getJumlahbarang() {
		return jumlahbarang;
	}

	public void setJumlahbarang(String jumlah) {
		this.jumlahbarang = jumlah;
	}

	public String getNamabarang() {
		return namabarang;
	}

	public void setNamabarang(String namabarang) {
		this.namabarang = namabarang;
	}

	public String getNamalengkap() {
		return namalengkap;
	}

	public void setNamalengkap(String namalengkap) {
		this.namalengkap = namalengkap;
	}

	public String getNotelp() {
		return notelp;
	}

	public void setNotelp(String notelp) {
		this.notelp = notelp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTanggal() {
		return tanggal;
	}

	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	
	
	
	
}
