package org.psi.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;


@Entity
public class Donasi {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_donatur() {
		return id_donatur;
	}
	public void setId_donatur(int id_donatur) {
		this.id_donatur = id_donatur;
	}

	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getNo_telp() {
		return no_telp;
	}
	public void setNo_telp(String no_telp) {
		this.no_telp = no_telp;
	}
	public String getNama_barang() {
		return nama_barang;
	}
	public void setNama_barang(String nama_barang) {
		this.nama_barang = nama_barang;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJenis_donasi() {
		return jenis_donasi;
	}
	public void setJenis_donasi(String jenis_donasi) {
		this.jenis_donasi = jenis_donasi;
	}
	
	public String getNama_lengkap() {
		return nama_lengkap;
	}
	public void setNama_lengkap(String nama_lengkap) {
		this.nama_lengkap = nama_lengkap;
	}
	
	public String getJumlah_barang() {
		return jumlah_barang;
	}
	public void setJumlah_barang(String jumlah_barang) {
		this.jumlah_barang = jumlah_barang;
	}
	public String getJumlah_uang() {
		return jumlah_uang;
	}
	public void setJumlah_uang(String jumlah_uang) {
		this.jumlah_uang = jumlah_uang;
	}

	private int id_donatur;
	@NotNull(message="Nama tidak boleh kosong")
	private String nama_lengkap;
	@NotNull(message="alamat tidak boleh kosong")
	private String alamat;
	
	@NotNull(message="Nomor Telepon tidak boleh kosong")
	private String no_telp;
	@NotNull(message="Nama Barang tidak boleh kosong")
	private String nama_barang;
	private String jumlah_barang;
	private String jumlah_uang;
	private String status;
	@NotNull(message=" Jenis Donasi Harus Dipilih")
	private String jenis_donasi;
	public Donasi() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	}
	 

