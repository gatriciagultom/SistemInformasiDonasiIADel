package org.psi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "donasi_uang")
public class DonasiUang {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "jumlah_uang")
	private String jumlahuang;
	
	@Column(name = "nama_lengkap")
	private String namalengkap;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "tanggal_kirim")
	private String tanggalkirim;
	
	@Column(name = "bukti")
	private String bukti;
	
	@Column(name= "id_donatur")
	private int iddonatur;
	
	@Column(name= "id_penerima")
	private String idpenerima;
	
	@Column(name = "no_telp")
	private String notelp;
	
	
	public int getIddonatur() {
		return iddonatur;
	}

	public void setIddonatur(int i) {
		this.iddonatur = i;
	}

	public String getIdpenerima() {
		return idpenerima;
	}

	public void setIdpenerima(String idpenerima) {
		this.idpenerima = idpenerima;
	}

	public String getNotelp() {
		return notelp;
	}

	public void setNotelp(String notelp) {
		this.notelp = notelp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJumlahuang() {
		return jumlahuang;
	}

	public void setJumlahuang(String jumlahuang) {
		this.jumlahuang = jumlahuang;
	}

	public String getNamalengkap() {
		return namalengkap;
	}

	public void setNamalengkap(String namalengkap) {
		this.namalengkap = namalengkap;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTanggalkirim() {
		return tanggalkirim;
	}

	public void setTanggalkirim(String tanggalkirim) {
		this.tanggalkirim = tanggalkirim;
	}

	public String getBukti() {
		return bukti;
	}

	public void setBukti(String string) {
		this.bukti = string;
	}
	
}
