package org.psi.dao;

import java.util.List;

import org.psi.models.Keuangan;

public interface KeuanganDao {
	List<Keuangan> getAllKeuangan();
	List<Keuangan> getAllKeuanganByIdAdmin(int id);
	List<Keuangan>getAllKeuanganByStatus(String status);
	Keuangan saveOrUpdate(Keuangan keuangan);
	Keuangan findKeuanganById(int id);
	void deleteKeuanganById(int id);
	void updateStatusKeuanganById(int id);

}
