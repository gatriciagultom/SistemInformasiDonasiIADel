package org.psi.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.psi.dao.DonasiDao;
import org.psi.models.Donasi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DonasiDaoImpl implements DonasiDao {

	private EntityManagerFactory emf;

	
	@Override
	public List<Donasi> getAllDonasi() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Donasi", Donasi.class).getResultList();
	}

	@Override
	public Donasi saveOrUpdate(Donasi donasi) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		donasi.setStatus("Undone");
		Donasi saved = em.merge(donasi);
		em.getTransaction().commit();
		em.close();
		return saved;
	}
	@Override
	public List<Donasi> getAllDonasiByStatus(String status) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Donasi where status='"+status+"'", Donasi.class).getResultList();
	}
	@Override
	public void updateStatusDonasiById(int id) {
		Donasi donasi = findDonasiById(id);
		donasi.setStatus("Done");
		donasi = saveOrUpdate(donasi);		
	}
	@Override
	public Donasi findDonasiById(int id) {
		EntityManager em = emf.createEntityManager();		
		return em.find(Donasi.class, id);
	}
	@Override
	public List<Donasi> getAllDonasiByIdDonatur(int id) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Donasi where id_donatur="+id, Donasi.class).getResultList();
	}
	public EntityManagerFactory getEmf() {
		return emf;
	}
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public void deleteDonasiById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Donasi.class, id));
		System.out.println(findDonasiById(id).getId());
		em.getTransaction().commit();
	}

	@Override
	public void konfirmasi(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Donasi donasi = em.find(Donasi.class, id);
		System.out.println(donasi.getId());
		if(donasi.getStatus().equals("Undone")){
			donasi.setStatus("Done");	
		}
		saveOrUpdate(donasi);
		em.getTransaction().commit();	
	}
}
