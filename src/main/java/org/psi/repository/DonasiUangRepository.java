package org.psi.repository;
import java.util.List;

import org.psi.models.DonasiUang;
import org.springframework.data.jpa.repository.JpaRepository;
public interface DonasiUangRepository extends JpaRepository <DonasiUang , Integer>{
	List<DonasiUang> findByiddonatur (int id);
}
